export const local = {
    Component: {
        Footer: {
            "seeSection": "Voir sur"
        },
        SnackBarComponent: {
            "undoButton": "Annuler"
        },
        AppDrawer: {
            translateButton: "Change to English",
            loginButton: "Connexion"
        }
    },
    Request: {
        error: {
            BadRequest: "Une erreur est survenue, veuillez recharger la page !",
            ErrorLogin: "Le token d'authentification a expiré, veuillez contacter un membre du SIA !",
            ErrorForbidden: "La ressource que vous tentez d'accèder vous est interdite !",
            ErrorNotFound: "Cette ressource est introuvable !",
            Default: "Une erreur est survenue, veuillez contacter un membre du SIA !",
        },
        sso: {
            waiting: "Chargement du module d'authentification...",
        }
    },
    Route: {
        "/": "Accueil",
        "/login": "Connexion",
        "/logout": "Déconnexion",
        "/profile": "Profil",
        "/admin": "Administration",
    },
    View: {
        AdminPanel: {
            "title": "Panel d'administration"
        }
    }
};