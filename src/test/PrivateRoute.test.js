import React from 'react';
import {useKeycloak} from "@react-keycloak/web";
import {loggedIn, loggedOut, notInit} from "./request/MockKeycloak";
import {ProtectedRoute} from "../component/ProtectedRoute";
import {local} from "../assets/lang/fr_fr";
import Adapter from "enzyme-adapter-react-16";
import Enzyme, {mount} from "enzyme";
import {Route, BrowserRouter as Router} from "react-router-dom";


// Mock the values returned by useKeycloak hook, to manage auth like we want
jest.mock("@react-keycloak/web");

Enzyme.configure({adapter: new Adapter()});

describe('Examining the rendering of the PrivateRoute Component', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('renders as waiting text when Keycloak is not init', () => {
        useKeycloak.mockReturnValue(notInit);
        const renderFn = jest.fn();
        const component = mount(<Router><ProtectedRoute roles={['role1']} render={renderFn} /></Router>)

        expect(renderFn).not.toBeCalled();
        expect(component.text()).toBe(local.Request.sso.waiting);
        component.unmount();
    });

    it('calls login function when not logged in', () => {
        const logged_dummy = loggedOut;
        logged_dummy.keycloak.login = jest.fn();

        useKeycloak.mockReturnValue(logged_dummy);
        const renderFn = jest.fn();
        const component = mount(<Router><ProtectedRoute roles={['role1']} render={renderFn} /></Router>)

        expect(renderFn).not.toBeCalled();
        expect(component.exists(Route)).toBe(true);
        expect(logged_dummy.keycloak.login).toBeCalled()
        component.unmount();
    });

    it('renders as an error for users who do not have the right roles', () => {
        useKeycloak.mockReturnValue(loggedIn);
        const renderFn = jest.fn();
        const component = mount(<Router><ProtectedRoute roles={['role-you-dont-have']} render={renderFn} /></Router>)

        expect(component.exists(Route)).toBe(true);
        expect(renderFn).not.toBeCalled();
        expect(component.find(Route).first().prop('render')().props.children).toEqual(local.Request.error.ErrorForbidden);
        component.unmount();
    })

    it('allows logged in users with the right roles to see the view', () => {
        useKeycloak.mockReturnValue(loggedIn);
        const renderFn = jest.fn();
        const component = mount(<Router><ProtectedRoute roles={['role1']} render={renderFn} /></Router>)

        expect(component.exists(Route)).toBe(true);
        expect(renderFn).toBeCalled();
        component.unmount();
    });
});