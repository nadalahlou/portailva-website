import React from 'react';
import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import isAuthorized, {canUseRoute} from "../utils/authorizations";
import {private_route, route} from "../route/Route";
jest.mock("@react-keycloak/web");
Enzyme.configure({adapter: new Adapter()});

describe('Testing the authorization util functions', () => {
    it('returns true or false whether the user is authorized or not', () => {
        expect(isAuthorized(['role1', 'role2'], {'portailva2-front': {roles:['role1']}})).toBe(true);
        expect(isAuthorized(['role0', 'role2'], {'portailva2-front': {roles:['role1']}})).toBe(false);
        expect(isAuthorized(['role1'], {'portailva2-front': {roles:[]}})).toBe(false);
    });

    it('returns the right boolean given a route and roles', () => {
        expect(canUseRoute(route[0], {'portailva2-front': {roles:[]}})).toBe(true);
        expect(canUseRoute(private_route[0][0], {'portailva2-front': {roles:[]}})).toBe(false);
        expect(canUseRoute(private_route[0][0], {'portailva2-front': {roles:['test_role']}})).toBe(true);
    })
});