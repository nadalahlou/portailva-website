export const notInit = {
    initialized: false
};

export const loggedOut = {
    keycloak: {
        authenticated: false,
        resourceAccess: {},
    },
    initialized: true
};

export const loggedIn = {
    keycloak: {
        authenticated: true,
        token: "kc_token",
        resourceAccess: {'portailva2-front': {roles:['role1']}},
    },
    initialized: true
};