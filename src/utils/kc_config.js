import Keycloak from "keycloak-js";

const config_kc = {
    realm: "asso-insa-lyon",
    "auth-server-url": process.env.REACT_APP_AUTH_URL,
    "ssl-required": "all",
    resource: "portailva2-front",
    "public-client": true,
    "confidential-port": 0,
    clientId: "portailva2-front",
    url: process.env.REACT_APP_AUTH_URL,
    "enable-cors": true,
    flow: 'implicit',
    enableLogging: true
};

const keycloak = new Keycloak(config_kc);
export default keycloak;