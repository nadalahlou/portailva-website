import React, {useEffect, useState} from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {MuiThemeProvider} from '@material-ui/core/styles';
import {CssBaseline, useMediaQuery} from "@material-ui/core";
import {ReactKeycloakProvider} from '@react-keycloak/web';

import AppDrawer from "../component/AppDrawer";
import Footer from "../component/Footer";
import SnackBarComponent from "../component/SnackBarComponent";
import {private_route as privateRouteList, route as routeList} from "../route/Route";
import keycloak from "../utils/kc_config";
import {getTheme} from "../Theme";
import {ProtectedRoute} from "../component/ProtectedRoute";

function App() {
    const userWantsDarkMode = useMediaQuery('(prefers-color-scheme: dark)');
    const [errorMessage, setError] = useState("");
    const [theme, setTheme] = useState(getTheme(userWantsDarkMode));

    const generatedRoute = routeList.map((list, listIndex) => {
        return (list.map((element, index) => {
            return <Route exact key={"route_" + listIndex + "_" + index}
                path={element.path}
                render={(props) => <element.render {...props} errorHandler={setError}/>}/>;
        }));
    });

    const protectedRoute = privateRouteList.map((list, listIndex) => {
        return (list.map((element, index) => {
            return <ProtectedRoute exact key={"private_route_" + listIndex + "_" + index}
                path={element.path} roles={element.roles}
                render={(props) => <element.render {...props} errorHandler={setError}/>}/>;
        }));
    });

    const toggleThemeModeChange = () => {
        setTheme(getTheme(theme.palette.type === "light"));
    };

    useEffect(() => setTheme(getTheme(userWantsDarkMode)), [userWantsDarkMode]);

    return (
        <div className="App">
            <MuiThemeProvider theme={theme}>
                <CssBaseline/>

                <ReactKeycloakProvider authClient={keycloak} >
                    <Router basename={process.env.PUBLIC_URL}>
                        <AppDrawer routes={routeList.concat(privateRouteList)} darkMode={theme.palette.type === "dark"}
                            changeThemeMode={toggleThemeModeChange}>
                            <Switch>
                                {generatedRoute}
                                {protectedRoute}
                            </Switch>
                            <Footer/>
                        </AppDrawer>
                    </Router>

                    {(errorMessage !== "") ? <SnackBarComponent type={"error"} message={errorMessage}/> : null}
                </ReactKeycloakProvider>
            </MuiThemeProvider>


        </div>
    );
}

export default App;
