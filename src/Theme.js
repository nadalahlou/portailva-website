import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import {grey, amber} from "@material-ui/core/colors";

export const getTheme = (prefersDarkMode) => {
    return createMuiTheme({
        palette: {
            type: prefersDarkMode ? 'dark' : 'light',
            primary: amber,
            secondary: grey
        }
    });
};
