import { rematchError } from '../requestErrors';

export const getHealthCheck = (privateRequest) => {
    return privateRequest.get("api/check/")
        .then(response => {
            return response.data;
        }).catch(rematchError);
};