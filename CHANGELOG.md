# Portail VA 2 - Front changelog

The latest version of this file can be found at the master branch of the Portail Va 2 repository (front).

## 0.1.1

### Removed (0 change)

### Fixed (0 change, 0 of them are from the community)

### Changed (1 change)
- Clean-up project

### Added (3 changes)
- Adding env switch
- Adding secret CI
- Adding SonarQube

### Other (0 change)

## 0.1.0

### Removed (0 change)

### Fixed (0 change, 0 of them are from the community)

### Changed (0 change)

### Added (3 changes)

- Adding base CRA webapp
- Adding basic views
- Adding proper CI/CD, linter config file and renovate to the project

### Other (0 change)
